# [DevOps Tech Radar](https://devopsradar.io/)

# Radar Areas
_______________________________________________________________________________
* **Dont's** - Things that we better not use/do anymore, e.g. micromanagement
* **Basic** - Things that are widely adopted, e.g. Git
* **Advanced** - Things that have been proven by tons of studies, but still have not become standard, e.g. SRE
* **Experimental** - Things that are experimental and have not been proven right or wrong but, when they work, bring great benefits, e.g. Backstage

# Radar Dimensions
_______________________________________________________________________________
### _Techniques_
>**Definition**: A way of carrying out a particular task, especially the execution or performance of a scientific procedure.<br>
**_Examples_**: [CI](https://en.wikipedia.org/wiki/Continuous_integration)/[CD](https://en.wikipedia.org/wiki/Continuous_deployment), [BDD](https://en.wikipedia.org/wiki/Behavior-driven_development), [TDD](https://en.wikipedia.org/wiki/Test-driven_development)

### _Software Tools_
>**Definition**: Tools that are used by engineers to build, maintain, debug or support other applications and programs - typically these tools are not instance/server based, but workflows can be triggered to finish after a certain time, e.g. running terraform plan or docker build.<br>
**_Examples_**: [Terraform](https://www.terraform.io/), [Git](https://git-scm.com/), [Docker](https://www.docker.com/)

### _Platforms_
>**Definition**: A model that creates value by facilitating exchanges between two or more interdependent groups, usually consumers and producers.<br>
**_Examples_**: [GitLab](https://about.gitlab.com/), [AWS](https://aws.amazon.com/), [Heroku](https://www.heroku.com/)

### _Frameworks & Practices_
>**Definition**: A basic structure underlying a system or concept & use of an idea, belief, or method, as opposed to theories that refer to it.<br>
**_Examples_**: [SAFe](https://en.wikipedia.org/wiki/Scaled_agile_framework), [SRE](https://en.wikipedia.org/wiki/Site_reliability_engineering), [Scrum](https://en.wikipedia.org/wiki/Scrum_(software_development))

# Guidelines
_______________________________________________________________________________
The following questions are designed to help radar owners sort and descope items:
* Is the element fulfilling the definition of the given dimension? **&rarr;** If not: Descoping
* Is the element DevOps relevant? **&rarr;** If not: Descoping
* Is the element a collective term? **&rarr;** If so: Is there a smaller element that would make sense to mention instead?

# Respect the Workflow
_______________________________________________________________________________
1.	You perform your changes on a `feature branch` and afterwards merge it back to the `master branch`. Please use the merge request functionality of GitLab.
2.	QA - We have a linter, but logical checks are not implemented, e.g. misspelling of a radar-dimension can lead to broken software.
3.	Therefore, check your output **before** merging it back to the `master branch`.

# Who are we?
_______________________________________________________________________________
As the active maintainers of the DevOps TechRadar, we are located at the heart of technological innovation: **Accenture** - DevOps community. Thanks to our diverse team of more than 80 members from the ASGR region, who come from a variety of technical backgrounds, we are able to look at concepts from different points of view.

# Why TechRadar?
_______________________________________________________________________________

With the creation of the TechRadar, we have founded a space where every tool relevant to DevOps finds its spot. Furthermore, it’s accessible to anyone who’s interested. On this central platform, the four different sectors: **Experimental**, **Advanced**, **Basic** and **Dont's** are presented in a structured layout. The positive aspect about the DevOps TechRadar is that it will be edited and thus be expanded over time. This way, we make it possible to offer the DevOps Addicts all over the world a broad tool stack about which they can inform themselves and contribute their ideas. In addition, the ability to rearrange the categorization of tools helps users view the products and concepts from different perspectives and obtain opinions from colleagues who are actively using them. Consequently, our community can follow market trends and is always up to date.

## How to preview your changes? 
1. Select your pipeline (job: test): <br><br> <img src="/docs/pipelineOverview.png"  width="50%" height="auto"> <br><br>
2. Download artifacts: <br><br> <img src="/docs/browseArtifacts.png"  width="20%" height="auto"> <br><br>
3. Extract the downloaded artifacts and click on the index.html to take a look `before` your merge back ;) <br><br> <img src="/docs/selectIndex.png"  width="80%" height="auto">


